#!/usr/bin/env python
# encoding: utf-8

from copy import copy
from heapq import heappush, heappop
from collections import defaultdict
from itertools import combinations, chain  # requires python 2.6+
import snap
import dispersion
import math


def swap(a, b):
    if a > b:
        return b, a
    return a, b


def dc(m, n):
    """partition density"""
    try:
        return m * (m - n + 1.0) / (n - 2.0) / (n - 1.0)
    except ZeroDivisionError:  # numerator is "strongly zero"
        return 0.0


class LDLC:
    def __init__(self, adj, edges, u, rec_disp):
        self.adj = adj  # node -> set of neighbors
        self.edges = edges  # list of edges
        self.u = u  # ego
        self.rec_disp = rec_disp  # recursive dispersion in u's ego network
        self.Mfactor = 2.0 / len(edges)
        self.edge2cid = {}
        self.cid2nodes, self.cid2edges = {}, {}
        self.orig_cid2edge = {}
        self.curr_maxcid = 0
        self.linkage = []  # dendrogram
        self.merges = 0
        self.cid2height = {}

        self.initialize_edges()  # every edge in its own comm
        self.D = 0.0  # partition density

    def initialize_edges(self):
        for cid, edge in enumerate(self.edges):
            edge = swap(*edge)  # just in case
            self.edge2cid[edge] = cid
            self.cid2edges[cid] = set([edge])
            self.orig_cid2edge[cid] = edge
            self.cid2nodes[cid] = set(edge)
        self.curr_maxcid = len(self.edges) - 1

    def merge_comms(self, edge1, edge2, S, dendro_flag=False, my_merges = 0):
        if edge1 == (None, None) or edge2 == (None, None):  # We'll get (None, None) at the end of clustering
            return my_merges
        cid1, cid2 = self.edge2cid[edge1], self.edge2cid[edge2]
        if cid1 == cid2:  # already merged!
            return my_merges
        m1, m2 = len(self.cid2edges[cid1]), len(self.cid2edges[cid2])
        n1, n2 = len(self.cid2nodes[cid1]), len(self.cid2nodes[cid2])
        size = min(n1, n2)

        Dc1, Dc2 = dc(m1, n1), dc(m2, n2)
        if m2 > m1:  # merge smaller into larger
            cid1, cid2 = cid2, cid1

        if dendro_flag:
            self.curr_maxcid += 1
            newcid = self.curr_maxcid
            self.cid2edges[newcid] = self.cid2edges[cid1] | self.cid2edges[cid2]
            self.cid2nodes[newcid] = set()
            for e in chain(self.cid2edges[cid1], self.cid2edges[cid2]):
                self.cid2nodes[newcid] |= set(e)
                self.edge2cid[e] = newcid
            del self.cid2edges[cid1], self.cid2nodes[cid1]
            del self.cid2edges[cid2], self.cid2nodes[cid2]
            m, n = len(self.cid2edges[newcid]), len(self.cid2nodes[newcid])

            self.linkage.append((cid1, cid2, S))

        else:
            self.cid2edges[cid1] |= self.cid2edges[cid2]
            for e in self.cid2edges[cid2]:  # move edges,nodes from cid2 to cid1
                self.cid2nodes[cid1] |= set(e)
                self.edge2cid[e] = cid1
            del self.cid2edges[cid2], self.cid2nodes[cid2]

            m, n = len(self.cid2edges[cid1]), len(self.cid2nodes[cid1])

            #height
            if cid1 in self.cid2height:
                self.cid2height[cid1] = self.cid2height[cid1] + 1
            else:
                self.cid2height[cid1] = 1
            if cid2 in self.cid2height:
                del self.cid2height[cid2]

        Dc12 = dc(m, n)
        self.D += (Dc12 - Dc1 - Dc2) * self.Mfactor  # update partition density
        if size < 3:
            my_merges += 1
        return my_merges

    def single_linkage(self, threshold=None, dendro_flag=False):
        #print "clustering..."
        self.list_D = [(1.0, 0.0)]  # list of (S_i,D_i) tuples...
        self.best_D = 0.0
        self.best_S = 1.0  # similarity threshold at best_D
        self.best_P = None  # best partition, dict: edge -> cid
        H = similarities_unweighted(self.adj, self.rec_disp)  # min-heap ordered by 1-s
        S_prev = -1

        # (1.0, (None, None)) takes care of the special case where the last
        # merging gives the maximum partition density (e.g. a single clique).
        for oms, (i, j, n) in chain(H, [(1.0, (None, None, None))]):
            S = (1 - oms)  # remember, H is a min-heap
            disp = dispersion.get_dispersion_from_dict(self.rec_disp, i, j, n)
            S_nD = S * disp  # use similarity for threshold check
            if threshold and S_nD < threshold:
                break

            if threshold == None:
                if S_nD != S_prev:  # update list
                    if self.D >= self.best_D:  # check PREVIOUS merger, because that's
                        self.best_D = self.D  # the end of the tie
                        self.best_S = S_nD
                        self.best_P = copy(self.edge2cid)  # slow...
                    self.list_D.append((S, self.D))
                    S_prev = S_nD
            self.merges += self.merge_comms(swap(i, n), swap(j, n), S, dendro_flag)
        if threshold != None:
            return self.edge2cid, self.D
        if dendro_flag:
            return self.best_P, self.best_S, self.best_D, self.list_D, self.orig_cid2edge, self.linkage
        else:
            return self.best_P, self.best_S, self.best_D, self.list_D


def similarities_unweighted(adj, rec_disp):
    """Get all the edge similarities. Input dict maps nodes to sets of neighbors.
    Output is a list of decorated edge-pairs, (1-sim,eij,eik), ordered by similarity.
    """
    #print "computing similarities..."
    i_adj = dict((n, adj[n] | set([n])) for n in adj)  # node -> inclusive neighbors

    min_heap = []  # elements are (1-sim,eij,eik)
    for n in adj:  # n is the shared node
        if len(adj[n]) > 1:
            for i, j in combinations(adj[n], 2):  # all unordered pairs of neighbors
                inc_ns_i, inc_ns_j = i_adj[i], i_adj[j]  # inclusive neighbors
                S = 1.0 * len(inc_ns_i & inc_ns_j) / len(inc_ns_i | inc_ns_j)  # Jacc similarity...
                disp = dispersion.get_dispersion_from_dict(rec_disp, i, j, n)
                S = S / disp
                heappush(min_heap, (1 - S, (i, j, n)))
    return [heappop(min_heap) for i in xrange(len(min_heap))]  # return ordered edge pairs


def read_edgelist_unweighted(filename, delimiter=None, nodetype=int):
    """reads two-column edgelist, returns dictionary
    mapping node -> set of neighbors and a list of edges
    """
    #adj = defaultdict(set)  # node to set of neighbors
    #edges = set()
    G = snap.TUNGraph.New()
    for line in open(filename, 'U'):
        L = line.strip().split(delimiter)
        ni, nj = nodetype(L[0]), nodetype(L[1])  # other columns ignored
        if ni != nj:  # skip any self-loops...
            if not G.IsNode(ni):
                G.AddNode(ni)
            if not G.IsNode(nj):
                G.AddNode(nj)
            G.AddEdge(ni,nj)
            #edges.add(swap(ni, nj))
            #adj[ni].add(nj)
            #adj[nj].add(ni)  # since undirected

    #degrees = []
    #for neighbors in adj.values():
    #    degrees.append(len(neighbors))
    #    degrees.append(len(neighbors))
    #print len(degrees)

    return G


def write_edge2cid(e2c, filename, delimiter="\t"):
    """writes the .edge2comm, .comm2edges, and .comm2nodes files"""

    # renumber community id's to be sequential, makes output file human-readable
    c2c = dict((c, i + 1) for i, c in enumerate(sorted(list(set(e2c.values())))))  # ugly...

    # write edge2cid three-column file:
    #f = open(filename + ".edge2comm.txt", 'w')
    #for e, c in sorted(e2c.iteritems(), key=itemgetter(1)):
    #    f.write("%s%s%s%s%s\n" % (str(e[0]), delimiter, str(e[1]), delimiter, str(c2c[c])))
    #f.close()

    cid2edges, cid2nodes = defaultdict(set), defaultdict(set)  # faster to recreate here than
    for edge, cid in e2c.iteritems():  # to keep copying all dicts
        cid2edges[cid].add(edge)  # during the linkage...
        cid2nodes[cid] |= set(edge)
    cid2edges, cid2nodes = dict(cid2edges), dict(cid2nodes)

    # write list of edges for each comm, each comm on its own line
    # first entry of each line is cid
    node_clusters = []
    #f, g = open(filename + ".comm2edges.txt", 'w'), open(filename + ".comm2nodes.txt", 'w')
    for cid in sorted(cid2edges.keys()):
        #strcid = str(c2c[cid])
        #nodes = map(str, cid2nodes[cid])
        #edges = ["%s,%s" % (ni, nj) for ni, nj in cid2edges[cid]]
        #f.write(delimiter.join([strcid] + edges))
        #f.write("\n")
        node_clusters.append(set(cid2nodes[cid]))
        #g.write(delimiter.join([strcid] + nodes))
        #g.write("\n")
    #f.close()
    #g.close()
    return node_clusters


def write_dendro(filename, orig_cid2edge, linkage):
    with open(filename + '.cid2edge.txt', 'w') as fout:
        for cid, e in orig_cid2edge.iteritems():
            fout.write("%d\t%s,%s\n" % (cid, str(e[0]), str(e[1])))

    with open(filename + '.linkage.txt', 'w') as fout:
        for x in linkage:
            fout.write('%s\n' % '\t'.join(map(str, x)))


def get_non_trivial_clusters(clusters):
    non_trivial_clusters = []
    for cluster in clusters:
        if len(cluster) > 2:
            non_trivial_clusters += [cluster]
    return non_trivial_clusters


def get_u_clusters(clusters, u):
    u_clusters = []
    for cluster in clusters:
        if u in cluster:
            u_clusters += [cluster]
        #else:
        #    cluster.add(u)
        #    u_clusters += [cluster]
    return u_clusters


def get_best_cluster(cluster, ground_truth_clusters):
    best_cluster = set()
    max_size = 0
    for ground_truth_cluster in ground_truth_clusters:
        common_elements = set.intersection(cluster, ground_truth_cluster)
        if len(common_elements) > max_size:
            max_size = len(common_elements)
            best_cluster = ground_truth_cluster
    return best_cluster


def get_argmaxf_cluster(cluster, ground_truth_clusters):
    best_f1 = 0.0
    best_gtc = None
    for ground_truth_cluster in ground_truth_clusters:
        f1 = get_f_score(cluster, ground_truth_cluster)
        if f1 > best_f1:
            best_f1 = f1
            best_gtc = ground_truth_cluster
    return best_f1, best_gtc


def get_precision(cluster1, cluster2):
    common_elements = cluster1.intersection(cluster2)
    return float(len(common_elements))/len(cluster1)


def get_recall(cluster1, cluster2):
    common_elements = cluster1.intersection(cluster2)
    return float(len(common_elements))/len(cluster2)


def get_f_score(cluster1, cluster2):
    precision = get_precision(cluster1, cluster2)
    recall = get_recall(cluster1, cluster2)
    if precision == 0 and recall == 0:
        return 0.0, 0.0
    return 2 * (precision * recall) / (precision + recall)


def get_average_f_score(clusters, ground_truth_clusters):
    print "Calculating average f1 score"
    first_sum = 0.0
    first_count = 0
    for ground_truth_cluster in ground_truth_clusters:
        f1 = get_argmaxf_cluster(ground_truth_cluster, clusters)
        first_sum += f1
        first_count += 1
    second_sum = 0.0
    second_count = 0
    for cluster in clusters:
        f1 = get_argmaxf_cluster(cluster, ground_truth_clusters)
        second_sum += f1
        second_count += 1
        return 0.5 * ((first_sum / first_count) + (second_sum / second_count))


def get_clusters_from_file_bigclam(filename, u):
    clusters = []
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            line_set = set()
            string = line.split('\t')
            for category in string:
                line_set.add(category)
            if u in line_set:
                clusters.append(line_set)
    return clusters


def get_clusters_from_file_bigclam(filename):
    clusters = []
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            line_set = set()
            string = line.split('\t')
            for category in string:
                line_set.add(int(category))
            clusters.append(line_set)
    return clusters


def get_dispersion(graph, normalized=False):
    """Get all the edge dispersion measures. Input dict maps nodes to sets of neighbors.
    Output is a list of decorated edge-pairs, (1-sim,eij,eik), ordered by similarity.
    """
    print "computing dispersion..."
    max_disp = 1
    min_disp = 1
    dispersion_dict = {None: 1}
    for n in graph:  # n is the shared node
        for i in graph[n]:
            d = dispersion.get_dispersion(graph, i, n)
            dispersion_dict[(i, n)] = d
            if max_disp < d:
                max_disp = d
            if min_disp > d:
                min_disp = d
    return dispersion_dict, max_disp, min_disp  # return dict with dispersion of edge pairs


def get_scaled_dispersion(Disp, max_disp, min_disp, high):
    low = 1
    scaled_dispersion = {}
    for key in Disp:
        scaled_dispersion[key] = math.log10(low + (Disp[key] - min_disp) * (high - low) / (max_disp - min_disp)) + 1
    return scaled_dispersion


def get_standardized_dispersion(Disp, stdev, mean, min_disp):
    standardized_dispersion = {}
    st_min_disp = -(min_disp - mean) / stdev + 1
    for key in Disp:
        standardized_dispersion[key] = (Disp[key] - mean) / stdev + st_min_disp
    return standardized_dispersion
