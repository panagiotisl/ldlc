__author__ = 'Panagiotis Liakos'

"""A module for the calculation of dispersion"""

from lru import LRU
import copy
import heapq
import random

#common_neighbors = LRU(1)


def swap(a, b):
    if a > b:
        return b, a
    return a, b


def swap_to_string(a, b):
    if a > b:
        return b+':'+a
    return a+':'+b


def get_common_neighbors(graph, a, b):
    return list(set(graph[a]).intersection(graph[b]))


#def get_or_find_common_neighbors(graph, a, b):
#    pair = swap_to_string(a, b)
#    try:
#        return common_neighbors[pair]
#    except KeyError:
#        cn = get_common_neighbors(graph, a, b)
#        common_neighbors[pair] = cn
#        return cn


#def get_or_find_common_neighbors_tuple(graph, a, b):
#    pair = swap(a, b)
#    try:
#        return common_neighbors[pair]
#    except KeyError:
#        cn = get_common_neighbors(graph, a, b)
#        common_neighbors[pair] = cn
#        return cn


def get_ego(u, graph):
    g_ego = dict()
    e_ego = set()
    g_ego[u] = copy.deepcopy(graph[u])
    neighbors = copy.deepcopy(g_ego[u])
    neighbors.add(u)
    for n in g_ego[u]:
        g_ego[n] = graph[n].intersection(neighbors)
        for s in g_ego[n]:
            e_ego.add(swap(n, s))
    return g_ego, e_ego


def get_ego_snap(u, graph):
    g_ego = dict()
    e_ego = set()
    NI = graph.GetNI(u)
    g_ego[u] = set()
    for Id in NI.GetOutEdges():
        g_ego[u].add(Id)
    neighbors = copy.deepcopy(g_ego[u])
    neighbors.add(u)
    for n in g_ego[u]:
        neighbor = graph.GetNI(n)
        temp = set()
        for Id in neighbor.GetOutEdges():
            temp.add(Id)
        g_ego[n] = temp.intersection(neighbors)
        for s in g_ego[n]:
            e_ego.add(swap(n, s))
    return g_ego, e_ego


def get_max_len_ego_snap(u, graph, max_len):
    g_ego, e_ego = get_ego_snap(u, graph)
    if len(g_ego) <= max_len:
        return g_ego, e_ego
    new_g_ego = dict()
    new_e_ego = set()
    temp_u = g_ego[u]
    del g_ego[u]
    sample = heapq.nlargest(max_len - 1, g_ego, key=lambda x: len(g_ego[x]))
    g_ego[u] = temp_u
    sample = set(sample)
    sample.add(u)
    for node in sample:
        new_g_ego[node] = set([x for x in g_ego[node] if x in sample])
    for (a, b) in e_ego:
        if a in new_g_ego and b in new_g_ego:
            new_e_ego.add((a, b))
    return new_g_ego, new_e_ego


def get_random_ego_snap(u, graph, max_len):
    g_ego, e_ego = get_ego_snap(u, graph)
    if len(g_ego) <= max_len:
        return g_ego, e_ego
    new_g_ego = dict()
    new_e_ego = set()
    temp_u = g_ego[u]
    del g_ego[u]
    sample = random.sample(g_ego, max_len)
    g_ego[u] = temp_u
    sample = set(sample)
    sample.add(u)
    for node in sample:
        new_g_ego[node] = set([x for x in g_ego[node] if x in sample])
    for (a, b) in e_ego:
        if a in new_g_ego and b in new_g_ego:
            new_e_ego.add((a, b))
    return new_g_ego, new_e_ego


def get_comm_ego_snap(u, graph, comm):
    g_ego = dict()
    e_ego = set()
    g_ego[u] = set()
    for Id in comm:
        if Id != u:
            g_ego[u].add(Id)
    neighbors = copy.deepcopy(g_ego[u])
    neighbors.add(u)
    for n in g_ego[u]:
        neighbor = graph.GetNI(n)
        temp = set()
        for Id in neighbor.GetOutEdges():
            temp.add(Id)
        g_ego[n] = temp.intersection(neighbors)
        for s in g_ego[n]:
            e_ego.add(swap(n, s))
    return g_ego, e_ego


def get_ego_cluster(u, cluster, graph):
    g_ego = dict()
    e_ego = set()
    g_ego[u] = cluster
    neighbors = copy.deepcopy(g_ego[u])
    neighbors.add(u)
    for n in graph[u]:
        g_ego[n] = graph[n].intersection(neighbors)
        for s in g_ego[n]:
            e_ego.add(swap(n, s))
    return g_ego, e_ego


def get_dispersion_from_dict(dispersion, i, j, n):
    try:
        disp = dispersion[i]
    except:
        disp = 0
    try:
        disp += dispersion[j]
    except:
        pass
    try:
        disp += dispersion[n]
    except:
        pass
    return 1 if disp == 0 else disp


def get_dispersion_u(u, ego_u):
    dispersion_u = dict()
    for v in ego_u[u]:
        dispersion = 0
        cn = get_common_neighbors(ego_u, u, v)
        length = len(cn)
        for i in xrange(0, length - 1):
            s = cn[i]
            sn = ego_u[s]
            for j in xrange(i+1, length):
                t = cn[j]
                if t not in sn:
                    st_cn = get_common_neighbors(ego_u, s, t)
                    if len(st_cn) <= 2:
                        dispersion += 1
        dispersion_u[v] = dispersion
    return dispersion_u


def get_dispersion_u_cluster(u, cluster, adj):
    max_dispersion = 0
    dispersion_u = dict()
    cluster.remove(u)
    g_ego_u, e_ego_u  = get_ego_cluster(u, cluster, adj)
    for v in cluster:
        dispersion = 0
        cn = get_common_neighbors(g_ego_u, u, v)
        length = len(cn)
        for i in xrange(0, length - 1):
            s = cn[i]
            sn = g_ego_u[s]
            for j in xrange(i+1, length):
                t = cn[j]
                if t not in sn:
                    st_cn = get_common_neighbors(g_ego_u, s, t)
                    if len(st_cn) <= 2:
                        dispersion += 1
        dispersion_u[v] = dispersion
        if dispersion > max_dispersion:
            max_dispersion = dispersion
    return dispersion_u, max_dispersion


def get_dispersion_u_cluster_lemon(u, cluster, graph_linklist):
    cluster = graph_linklist[u].intersection(cluster)
    max_dispersion = 0
    dispersion_u = dict()
    cluster.remove(u)
    g_ego_u, e_ego_u = get_ego_cluster(u, cluster, graph_linklist)
    for v in cluster:
        dispersion = 0
        cn = get_common_neighbors(g_ego_u, u, v)
        length = len(cn)
        for i in xrange(0, length - 1):
            s = cn[i]
            sn = g_ego_u[s]
            for j in xrange(i+1, length):
                t = cn[j]
                if t not in sn:
                    st_cn = get_common_neighbors(g_ego_u, s, t)
                    if len(st_cn) <= 2:
                        dispersion += 1
        dispersion_u[v] = dispersion
        if dispersion > max_dispersion:
            max_dispersion = dispersion
    return dispersion_u, max_dispersion


def get_recursive_dispersion_u(u, ego_u):
    #print "computing recursive dispersion..."
    max_disp = 1
    min_disp = 1
    x = {None: 0.5}
    temp_dict = {None: 0.5}
    for v in ego_u[u]:
        x[v] = 1
    for iter in xrange(3):
        #print 'Iteration', iter
        for v in ego_u[u]:
            cn = get_common_neighbors(ego_u, u, v)
            first = 0.0
            second = 0.0
            for w in cn:
                first += x[w]**2
            for i in xrange(len(cn)-1):
                s = cn[i]
                sn = ego_u[s]
                for j in xrange(i+1, len(cn)):
                    t = cn[j]
                    if t not in sn:
                        st_cn = get_common_neighbors(ego_u, s, t)
                        if len(st_cn) <= 2:
                            second += x[s]*x[t]
            value = float(first + 2 * second) / max(len(cn), 1)
            temp_dict[v] = value
            if (iter == 2) and (value > max_disp):
                max_disp = value
            if (iter == 2) and (value < min_disp):
                min_disp = value
        x = temp_dict
        temp_dict = {None: 0.5}

    values = x.values()
    #print "MEAN_DISP ", statistics.mean(values)
    #print "STDEV_DISP ", statistics.stdev(values)
    #print "PSTDEV_DISP ", statistics.pstdev(values)
    #print "MAX_DISP ", max(values)
    #print "MIN_DISP ", min(values)
    return x, max_disp, min_disp # return dict with dispersion of edge pairs


def get_dispersion(graph, a, b):
    dispersion = 0
    cn = get_common_neighbors(graph, a, b)
    length = len(cn)
#       for all pairs of common neighbors calculate the distance (zero or one)
    for i in xrange(0, length - 1):
        s = cn[i]
        sn = graph[s]
        for j in xrange(i+1, length):
            t = cn[j]
            if t not in sn:
                st_cn = get_common_neighbors(graph, s, t)
                st_length = len(st_cn)
                if st_length <= 2:
                    dispersion += 1
    return max(dispersion, 1)


def get_normalized_dispersion_norm(graph, a, b):
    dispersion = 0
#       get common neighbors
    cn = get_common_neighbors(graph, a, b)
#       for all pairs of common neighbors calculate the distance (zero or one)
    for i in xrange(0, cn.len - 1):
        s = cn[i]
        sn = graph[s]
        for j in xrange(i+1, cn.len):
            t = cn[j]
            if t not in sn and get_common_neighbors(graph, s, t).len <= 2:
                dispersion += 1
    return float(dispersion)/cn.len


def get_normal_dispersion(graph, normalized=False):
    """Get all the edge dispersion measures. Input dict maps nodes to sets of neighbors.
    Output is a list of decorated edge-pairs, (1-sim,eij,eik), ordered by similarity.
    """
    print "computing dispersion..."
    dispersion_dict = {None: 0.5}
    count = 0
    for n in graph:  # n is the shared node
        for i in graph[n]:
            count += 1
            d = get_dispersion(graph, i, n)
            dispersion_dict[(swap_to_string(i, n))] = d
    return dispersion_dict  # return dict with dispersion of edge pairs


def get_or_find_dispersion(graph, dispersion, a, b):
    #pair = swap_to_string(a, b)
    pair = a+':'+b
    if pair in dispersion:
        return dispersion[pair]
    else:
        dispersion[pair] = get_dispersion(graph, a, b)
        return dispersion[pair]


def get_recursive_dispersion(graph):
    print "computing recursive dispersion..."
    #normal_dispersion = get_normal_dispersion(graph)
    max_disp = 1
    min_disp = 1
    normal_dispersion = {}
    dispersion_dict = {None: 0.5}
    temp_dict = {None: 0.5}
    for u in graph:  # n is the shared node
        for v in graph[u]:
            dispersion_dict[(swap_to_string(u, v))] = 1
    for iter in xrange(3):
        print 'Iteration', iter
        disp_pairs = set()
        for u in graph:  # n is the shared node
            for v in graph[u]:
                pair = swap_to_string(v, u)
                #
                if pair in disp_pairs:
                    continue
                disp_pairs.add(pair)
                c_uv = get_common_neighbors(graph, v, u)
                first = 0.0
                second = 0.0
                for w in c_uv:
                    first += dispersion_dict[(swap_to_string(u, w))]
                for s in xrange(len(c_uv)-1):
                    for t in xrange(s+1, len(c_uv)):
                        second += get_or_find_dispersion(graph, normal_dispersion, c_uv[s], c_uv[t]) * dispersion_dict[(swap_to_string(u, c_uv[s]))] * dispersion_dict[(swap_to_string(u, c_uv[t]))]
                value = float(first + 2 * second) / max(len(c_uv), 1)
                temp_dict[pair] = value
                if (iter == 2) and (value > max_disp):
                    max_disp = value
                if (iter == 2) and (value < min_disp):
                    min_disp = value
        #temp = dispersion_dict
        dispersion_dict = temp_dict
        temp_dict = {None: 0.5}
        #temp_dict = temp

    values = dispersion_dict.values()
    #print "MEAN_DISP ", statistics.mean(values)
    #print "STDEV_DISP ", statistics.stdev(values)
    #print "PSTDEV_DISP ", statistics.pstdev(values)
    print "MAX_DISP ", max(values)
    print "MIN_DISP ", min(values)
    return dispersion_dict, max_disp, min_disp # return dict with dispersion of edge pairs


def get_max_dispersion(u, comm):
    g_ego_u, e_ego_u = comm
    dispersion_u = get_dispersion_u(u, g_ego_u)
    return max(dispersion_u.values())


def get_mean_dispersion(u, comm):
    g_ego_u, e_ego_u = comm
    dispersion_u = get_dispersion_u(u, g_ego_u)
    return float(sum(dispersion_u.values())) / max(len(dispersion_u.values()), 1)
