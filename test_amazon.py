import os
import dispersion
import ldlcsnap
import time

delimiter = '\t'

edgelist_filename = 'amazon/com-amazon.ungraph.txt'
ground_truth_clusters_filename = 'amazon/com-amazon.top5000.cmty.txt'

basename = os.path.splitext(edgelist_filename)[0]
G = ldlcsnap.read_edgelist_unweighted(edgelist_filename, delimiter=delimiter)

ground_truth_clusters = ldlcsnap.get_clusters_from_file_bigclam(ground_truth_clusters_filename)
allTime = 0.0
allF1 = 0.0
allHeight = 0.0
count = 0
for gtc in ground_truth_clusters:
    f1_best = 0.0
    comm = None
    best_time = None
    for u in gtc:
        startTime = time.time()
        g_ego_u, e_ego_u = dispersion.get_ego_snap(u, G)
        #g_ego_u, e_ego_u = dispersion.get_max_len_ego_snap(u, G, 100)
        #g_ego_u, e_ego_u = dispersion.get_random_ego_snap(u, G, 100)
        rec_disp, max, min = dispersion.get_recursive_dispersion_u(u, g_ego_u)
        ldlc = ldlcsnap.LDLC(g_ego_u, e_ego_u, u, rec_disp)
        best_P, best_S, best_D, list_D = ldlc.single_linkage()
        clusters = ldlcsnap.write_edge2cid(best_P, "%s_maxS%f_maxD%f" % (basename, best_S, best_D), delimiter=delimiter)
        clusters = ldlcsnap.get_non_trivial_clusters(clusters)
        clusters = ldlcsnap.get_u_clusters(clusters, u)
        endTime = time.time()
        for cluster in clusters:
            best_f1, best_gtc = ldlcsnap.get_argmaxf_cluster(cluster, [gtc])
            if best_f1 > f1_best:
                f1_best = best_f1
                comm = cluster
                best_time = endTime - startTime
                best_height = ldlc.cid2height.itervalues().next()
            if best_f1 == 1.0:
                break
    if comm is not None:
        count += 1
        allTime += best_time
        allF1 += f1_best
        allHeight += best_height
        print f1_best, comm, gtc, allTime, u, best_height

print 'Avg. F1: ', allF1/count
print 'Avg. time: ', allTime/count
print 'Avg. height: ', allHeight/count
